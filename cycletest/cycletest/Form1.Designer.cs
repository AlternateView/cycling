﻿namespace cycletest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.openFD = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Line = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUS = new System.Windows.Forms.Button();
            this.btnEU = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnSPD = new System.Windows.Forms.Button();
            this.btnCAD = new System.Windows.Forms.Button();
            this.btnBPM = new System.Windows.Forms.Button();
            this.btnALT = new System.Windows.Forms.Button();
            this.btnPOW = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtSet = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.btnSet = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Speed2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cadence2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Altitude2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HeartRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Power2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(246, 108);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(354, 44);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFD
            // 
            this.openFD.FileName = "openFD";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(756, 814);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(273, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Average Heart Rate (BPM):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 946);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Average Speed (KM/H):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(756, 1079);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(257, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Average Cadence (RPM):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(756, 990);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Average Altitude (FT):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(756, 902);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(231, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Average Power (WAT):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1072, 814);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(506, 946);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 25);
            this.label8.TabIndex = 9;
            this.label8.Text = "label8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1072, 1079);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1072, 990);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 25);
            this.label10.TabIndex = 11;
            this.label10.Text = "label10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1072, 902);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 25);
            this.label11.TabIndex = 12;
            this.label11.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(144, 769);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 25);
            this.label12.TabIndex = 13;
            this.label12.Text = "Date:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(144, 814);
            this.label13.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(116, 25);
            this.label13.TabIndex = 14;
            this.label13.Text = "Start Time:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(144, 858);
            this.label14.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 25);
            this.label14.TabIndex = 15;
            this.label14.Text = "Length:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(144, 902);
            this.label15.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(296, 25);
            this.label15.TabIndex = 16;
            this.label15.Text = "Recording Interval (Seconds):";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(506, 769);
            this.label16.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 25);
            this.label16.TabIndex = 17;
            this.label16.Text = "label16";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(506, 814);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 25);
            this.label17.TabIndex = 18;
            this.label17.Text = "label17";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(506, 858);
            this.label18.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 25);
            this.label18.TabIndex = 19;
            this.label18.Text = "label18";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(506, 902);
            this.label19.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 25);
            this.label19.TabIndex = 20;
            this.label19.Text = "label19";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(144, 1035);
            this.label20.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(295, 25);
            this.label20.TabIndex = 21;
            this.label20.Text = "Total Distance Covered (KM):";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(144, 990);
            this.label21.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(251, 25);
            this.label21.TabIndex = 22;
            this.label21.Text = "Maximum Speed (KM/H):";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(756, 858);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(285, 25);
            this.label22.TabIndex = 23;
            this.label22.Text = "Maximum Heart Rate (BPM):";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(756, 769);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(279, 25);
            this.label23.TabIndex = 24;
            this.label23.Text = "Minimum Heart Rate (BPM):";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(756, 946);
            this.label24.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(243, 25);
            this.label24.TabIndex = 25;
            this.label24.Text = "Maximum Power (WAT):";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(756, 1035);
            this.label25.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(234, 25);
            this.label25.TabIndex = 26;
            this.label25.Text = "Maximum Altitude (FT):";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(506, 1035);
            this.label26.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(82, 25);
            this.label26.TabIndex = 27;
            this.label26.Text = "label26";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(506, 990);
            this.label27.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 25);
            this.label27.TabIndex = 28;
            this.label27.Text = "label27";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(1072, 858);
            this.label28.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(82, 25);
            this.label28.TabIndex = 29;
            this.label28.Text = "label28";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(1072, 769);
            this.label29.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(82, 25);
            this.label29.TabIndex = 30;
            this.label29.Text = "label29";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(1072, 946);
            this.label30.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 25);
            this.label30.TabIndex = 31;
            this.label30.Text = "label30";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(1072, 1035);
            this.label31.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 25);
            this.label31.TabIndex = 32;
            this.label31.Text = "label31";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(246, 108);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(354, 44);
            this.button2.TabIndex = 33;
            this.button2.Text = "Reset";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Line,
            this.Time,
            this.Speed,
            this.Cadence,
            this.Altitude,
            this.HeartRate,
            this.Power});
            this.dataGridView1.Location = new System.Drawing.Point(15, 374);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1296, 377);
            this.dataGridView1.TabIndex = 34;
            // 
            // Line
            // 
            this.Line.Frozen = true;
            this.Line.HeaderText = "Line";
            this.Line.Name = "Line";
            // 
            // Time
            // 
            this.Time.Frozen = true;
            this.Time.HeaderText = "Time";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            // 
            // Speed
            // 
            this.Speed.Frozen = true;
            this.Speed.HeaderText = "Speed (KM/H)";
            this.Speed.Name = "Speed";
            this.Speed.ReadOnly = true;
            // 
            // Cadence
            // 
            this.Cadence.Frozen = true;
            this.Cadence.HeaderText = "Cadence (RPM)";
            this.Cadence.Name = "Cadence";
            this.Cadence.ReadOnly = true;
            // 
            // Altitude
            // 
            this.Altitude.Frozen = true;
            this.Altitude.HeaderText = "Altitude (FT)";
            this.Altitude.Name = "Altitude";
            this.Altitude.ReadOnly = true;
            // 
            // HeartRate
            // 
            this.HeartRate.Frozen = true;
            this.HeartRate.HeaderText = "Heart Rate (BPM)";
            this.HeartRate.Name = "HeartRate";
            this.HeartRate.ReadOnly = true;
            // 
            // Power
            // 
            this.Power.Frozen = true;
            this.Power.HeaderText = "Power (WAT)";
            this.Power.Name = "Power";
            this.Power.ReadOnly = true;
            // 
            // btnUS
            // 
            this.btnUS.Image = ((System.Drawing.Image)(resources.GetObject("btnUS.Image")));
            this.btnUS.Location = new System.Drawing.Point(233, 214);
            this.btnUS.Margin = new System.Windows.Forms.Padding(6);
            this.btnUS.Name = "btnUS";
            this.btnUS.Size = new System.Drawing.Size(150, 96);
            this.btnUS.TabIndex = 35;
            this.btnUS.UseVisualStyleBackColor = true;
            this.btnUS.Click += new System.EventHandler(this.btnUS_Click);
            // 
            // btnEU
            // 
            this.btnEU.Image = ((System.Drawing.Image)(resources.GetObject("btnEU.Image")));
            this.btnEU.Location = new System.Drawing.Point(445, 214);
            this.btnEU.Margin = new System.Windows.Forms.Padding(6);
            this.btnEU.Name = "btnEU";
            this.btnEU.Size = new System.Drawing.Size(166, 96);
            this.btnEU.TabIndex = 36;
            this.btnEU.UseVisualStyleBackColor = true;
            this.btnEU.Click += new System.EventHandler(this.btnEU_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(229, 316);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(391, 25);
            this.label1.TabIndex = 37;
            this.label1.Text = "Please reset to enable region selection.";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(51, 241);
            this.label32.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(146, 37);
            this.label32.TabIndex = 38;
            this.label32.Text = "US (KM)";
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(1345, 35);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Legend = "Legend1";
            series1.Name = "Speed";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Legend = "Legend1";
            series2.Name = "Cadence";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Altitude";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Heart Rate";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series5.Legend = "Legend1";
            series5.Name = "Power";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(1522, 677);
            this.chart1.TabIndex = 39;
            this.chart1.Text = "chart1";
            this.chart1.SelectionRangeChanging += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CursorEventArgs>(this.chart1_SelectionRangeChanging);
            this.chart1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseUp);
            // 
            // btnSPD
            // 
            this.btnSPD.Location = new System.Drawing.Point(1724, 789);
            this.btnSPD.Name = "btnSPD";
            this.btnSPD.Size = new System.Drawing.Size(220, 75);
            this.btnSPD.TabIndex = 40;
            this.btnSPD.Text = "Speed (KM/H)";
            this.btnSPD.UseVisualStyleBackColor = true;
            this.btnSPD.Click += new System.EventHandler(this.btnSpeed_Click);
            // 
            // btnCAD
            // 
            this.btnCAD.Location = new System.Drawing.Point(1950, 789);
            this.btnCAD.Name = "btnCAD";
            this.btnCAD.Size = new System.Drawing.Size(220, 75);
            this.btnCAD.TabIndex = 41;
            this.btnCAD.Text = "Cadence (RPM)";
            this.btnCAD.UseVisualStyleBackColor = true;
            this.btnCAD.Click += new System.EventHandler(this.btnCAD_Click);
            // 
            // btnBPM
            // 
            this.btnBPM.Location = new System.Drawing.Point(1498, 789);
            this.btnBPM.Name = "btnBPM";
            this.btnBPM.Size = new System.Drawing.Size(220, 75);
            this.btnBPM.TabIndex = 42;
            this.btnBPM.Text = "Heart Rate (BPM)";
            this.btnBPM.UseVisualStyleBackColor = true;
            this.btnBPM.Click += new System.EventHandler(this.btnBPM_Click);
            // 
            // btnALT
            // 
            this.btnALT.Location = new System.Drawing.Point(2176, 789);
            this.btnALT.Name = "btnALT";
            this.btnALT.Size = new System.Drawing.Size(220, 75);
            this.btnALT.TabIndex = 43;
            this.btnALT.Text = "Altitude (FT)";
            this.btnALT.UseVisualStyleBackColor = true;
            this.btnALT.Click += new System.EventHandler(this.btnALT_Click);
            // 
            // btnPOW
            // 
            this.btnPOW.Location = new System.Drawing.Point(2402, 789);
            this.btnPOW.Name = "btnPOW";
            this.btnPOW.Size = new System.Drawing.Size(220, 75);
            this.btnPOW.TabIndex = 44;
            this.btnPOW.Text = "Power (WAT)";
            this.btnPOW.UseVisualStyleBackColor = true;
            this.btnPOW.Click += new System.EventHandler(this.btnPOW_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(1708, 726);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(821, 25);
            this.label33.TabIndex = 45;
            this.label33.Text = "Speed values multiplied by 10 to give an accurate representation of speed over ti" +
    "me.";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1640, 324);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(763, 49);
            this.textBox1.TabIndex = 46;
            this.textBox1.Text = "Please load a file to view data graph.";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(2612, 298);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(219, 344);
            this.textBox2.TabIndex = 47;
            this.textBox2.Text = "Click and drag the left mouse button to select a portion of the data. Right click" +
    " to zoom out one step.";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(1689, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(82, 25);
            this.label34.TabIndex = 48;
            this.label34.Text = "label34";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1830, 9);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 25);
            this.label35.TabIndex = 49;
            this.label35.Text = "label35";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1993, 9);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(82, 25);
            this.label36.TabIndex = 50;
            this.label36.Text = "label36";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(2139, 9);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(82, 25);
            this.label37.TabIndex = 51;
            this.label37.Text = "label37";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(156, 1301);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(192, 25);
            this.label38.TabIndex = 52;
            this.label38.Text = "Normalised Power:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(518, 1301);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(82, 25);
            this.label39.TabIndex = 53;
            this.label39.Text = "label39";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(156, 1256);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(286, 25);
            this.label40.TabIndex = 54;
            this.label40.Text = "Functional Threshold Power:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(518, 1257);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 25);
            this.label41.TabIndex = 55;
            this.label41.Text = "label41";
            // 
            // txtSet
            // 
            this.txtSet.Location = new System.Drawing.Point(650, 1183);
            this.txtSet.Name = "txtSet";
            this.txtSet.Size = new System.Drawing.Size(100, 31);
            this.txtSet.TabIndex = 56;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(156, 1189);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(447, 25);
            this.label42.TabIndex = 57;
            this.label42.Text = "Enter your functional theshold power in watts:";
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(773, 1179);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(354, 44);
            this.btnSet.TabIndex = 58;
            this.btnSet.Text = "Set / Calculate Power Zones";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(817, 1257);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(165, 25);
            this.label43.TabIndex = 59;
            this.label43.Text = "Intensity Factor:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(1084, 1256);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(82, 25);
            this.label44.TabIndex = 60;
            this.label44.Text = "label44";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(817, 1301);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(219, 25);
            this.label45.TabIndex = 61;
            this.label45.Text = "Training Stress Score";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(1084, 1301);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(82, 25);
            this.label46.TabIndex = 62;
            this.label46.Text = "label46";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(631, 1247);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(165, 79);
            this.button3.TabIndex = 63;
            this.button3.Text = "Calculate IF / TSS";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(365, 1492);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(159, 25);
            this.label47.TabIndex = 64;
            this.label47.Text = "Enter your age:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(775, 1492);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(352, 25);
            this.label48.TabIndex = 65;
            this.label48.Text = "Enter your resting heart rate (BPM):";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(544, 1489);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 31);
            this.textBox3.TabIndex = 66;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(1133, 1490);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 31);
            this.textBox4.TabIndex = 67;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1239, 1483);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(103, 44);
            this.button5.TabIndex = 69;
            this.button5.Text = "Set";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(156, 1568);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(56, 25);
            this.label49.TabIndex = 70;
            this.label49.Text = "Age:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(156, 1612);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(200, 25);
            this.label50.TabIndex = 71;
            this.label50.Text = "Resting Heart Rate:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(364, 1569);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(82, 25);
            this.label51.TabIndex = 72;
            this.label51.Text = "label51";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(364, 1612);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(82, 25);
            this.label52.TabIndex = 73;
            this.label52.Text = "label52";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(483, 1569);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(326, 60);
            this.button6.TabIndex = 74;
            this.button6.Text = "Calculate Heart Rate Zones";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(149, 1078);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(135, 25);
            this.label54.TabIndex = 77;
            this.label54.Text = "Weight (KG):";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(506, 1078);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(82, 25);
            this.label55.TabIndex = 78;
            this.label55.Text = "label55";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(666, 1483);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(103, 44);
            this.button4.TabIndex = 79;
            this.button4.Text = "Set";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(68, 1492);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(89, 25);
            this.label53.TabIndex = 80;
            this.label53.Text = "Gender:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBox1.Location = new System.Drawing.Point(213, 1489);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 33);
            this.comboBox1.TabIndex = 81;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(850, 1568);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(234, 25);
            this.label56.TabIndex = 82;
            this.label56.Text = "Max Heart Rate (BPM):";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(1105, 1568);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(82, 25);
            this.label57.TabIndex = 83;
            this.label57.Text = "label57";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(34, 1720);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(187, 25);
            this.label58.TabIndex = 84;
            this.label58.Text = "Heart Rate Zones:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(252, 1685);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(1251, 25);
            this.label59.TabIndex = 85;
            this.label59.Text = "1: Recovery (aerobic)        2: Endurance (aerobic)        3: Stamina (aerobic)  " +
    "      4: Economy (anaerobic)        5: Speed (anaerobic)";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(252, 1720);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(82, 25);
            this.label60.TabIndex = 86;
            this.label60.Text = "label60";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(506, 1720);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(82, 25);
            this.label61.TabIndex = 87;
            this.label61.Text = "label61";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(777, 1720);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(82, 25);
            this.label62.TabIndex = 88;
            this.label62.Text = "label62";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(1018, 1720);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(82, 25);
            this.label63.TabIndex = 89;
            this.label63.Text = "label63";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(1292, 1720);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(82, 25);
            this.label64.TabIndex = 90;
            this.label64.Text = "label64";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(68, 1416);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(144, 25);
            this.label65.TabIndex = 91;
            this.label65.Text = "Power Zones:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(242, 1381);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(1051, 25);
            this.label66.TabIndex = 92;
            this.label66.Text = "1: Active Recovery    2: Endurance    3: Tempo    4: Lactate Threshold    5: VO2 " +
    "Max    6: Anaerobic Capacity";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(279, 1416);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(82, 25);
            this.label67.TabIndex = 93;
            this.label67.Text = "label67";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(454, 1416);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(82, 25);
            this.label68.TabIndex = 94;
            this.label68.Text = "label68";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(600, 1416);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(82, 25);
            this.label69.TabIndex = 95;
            this.label69.Text = "label69";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(768, 1416);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(82, 25);
            this.label70.TabIndex = 96;
            this.label70.Text = "label70";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(939, 1416);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(82, 25);
            this.label71.TabIndex = 97;
            this.label71.Text = "label71";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(1097, 1416);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(82, 25);
            this.label72.TabIndex = 98;
            this.label72.Text = "label72";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Speed2,
            this.Cadence2,
            this.Altitude2,
            this.HeartRate2,
            this.Power2});
            this.dataGridView2.Location = new System.Drawing.Point(1539, 956);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 33;
            this.dataGridView2.Size = new System.Drawing.Size(1078, 277);
            this.dataGridView2.TabIndex = 100;
            // 
            // Speed2
            // 
            this.Speed2.HeaderText = "Speed (KM/H)";
            this.Speed2.Name = "Speed2";
            // 
            // Cadence2
            // 
            this.Cadence2.HeaderText = "Cadence (RPM)";
            this.Cadence2.Name = "Cadence2";
            // 
            // Altitude2
            // 
            this.Altitude2.HeaderText = "Altitude (FT)";
            this.Altitude2.Name = "Altitude2";
            // 
            // HeartRate2
            // 
            this.HeartRate2.HeaderText = "Heart Rate (BPM)";
            this.HeartRate2.Name = "HeartRate2";
            // 
            // Power2
            // 
            this.Power2.HeaderText = "Power (WAT)";
            this.Power2.Name = "Power2";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(1928, 913);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(257, 25);
            this.label73.TabIndex = 101;
            this.label73.Text = "Data from chart selection:";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(1467, 1256);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(329, 25);
            this.label74.TabIndex = 102;
            this.label74.Text = "Selected Average Speed (KM/H):";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(1467, 1315);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(347, 25);
            this.label75.TabIndex = 103;
            this.label75.Text = "Selected Average Cadence (RPM):";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(2068, 1260);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(312, 25);
            this.label76.TabIndex = 104;
            this.label76.Text = "Selected Average Altitude (FT):";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(2068, 1315);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(363, 25);
            this.label77.TabIndex = 105;
            this.label77.Text = "Selected Average Heart Rate (BPM):";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(1467, 1371);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(321, 25);
            this.label78.TabIndex = 106;
            this.label78.Text = "Selected Average Power (WAT):";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(1820, 1259);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(82, 25);
            this.label79.TabIndex = 107;
            this.label79.Text = "label79";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(1821, 1314);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(82, 25);
            this.label80.TabIndex = 108;
            this.label80.Text = "label80";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(1811, 1370);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(82, 25);
            this.label81.TabIndex = 109;
            this.label81.Text = "label81";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(2408, 1259);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(82, 25);
            this.label82.TabIndex = 110;
            this.label82.Text = "label82";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(2438, 1314);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(82, 25);
            this.label83.TabIndex = 111;
            this.label83.Text = "label83";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(876, 35);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 112;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2916, 1841);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.txtSet);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.btnPOW);
            this.Controls.Add(this.btnALT);
            this.Controls.Add(this.btnBPM);
            this.Controls.Add(this.btnCAD);
            this.Controls.Add(this.btnSPD);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEU);
            this.Controls.Add(this.btnUS);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Cycling Application";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button2;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnUS;
        private System.Windows.Forms.Button btnEU;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btnSPD;
        private System.Windows.Forms.Button btnCAD;
        private System.Windows.Forms.Button btnBPM;
        private System.Windows.Forms.Button btnALT;
        private System.Windows.Forms.Button btnPOW;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtSet;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.DataGridViewTextBoxColumn Line;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Speed2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cadence2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Altitude2;
        private System.Windows.Forms.DataGridViewTextBoxColumn HeartRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Power2;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
    }
}

