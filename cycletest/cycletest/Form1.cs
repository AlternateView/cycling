﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Runtime.InteropServices;

namespace cycletest
{
    public partial class Form1 : Form
    {

        bool US = true; // US mode, kilometres
        bool EU = false; // EU mode, miles
        bool showspeed = true; bool showcadence = true; bool showaltitude = true; bool showheartrate = true; bool showpower = true; // show or not show these on graph

        double FTP = 0; // functional threshold power
        double ppower = 0; // precise power
        double avgppower = 0; // average precise power
        double totalppower = 0; // total ppower used to average
        double NP = 0; // normalised power
        double IF = 0; // intensity factor
        double TSS = 0; // training stress score

        double percent = 0; /* 1% of HR used to calculate hr zones */ double zone1lower = 0; double zone2lower = 0;  double zone3lower = 0; double zone4lower = 0;
        double zone5lower = 0; double zone1upper = 0; double zone2upper = 0; double zone3upper = 0; double zone4upper = 0; double zone5upper = 0;

        double percent2 = 0; /* 1% of FTP used to calculate power zones */ double Zone1lower = 0; double Zone2lower = 0; double Zone3lower = 0; double Zone4lower = 0;
        double Zone5lower = 0; double Zone6lower = 0; double Zone2upper = 0; double Zone3upper = 0; double Zone4upper = 0; double Zone5upper = 0; double Zone6upper = 0;

        int RHR = 0; // resting heart rate
        int age = 0; // user age
        double maxHR = 0; // precise calculated max heart rate
        int dataline = 0; // lines in datagridview
        int weight = 0; // weight in KG
        int i = 0; // interval
        int seconds = 0; // total seconds in run

        public Form1()
        {
            InitializeComponent();
            label1.Visible = false; /* hide labels when nothing is loaded */ label7.Visible = false; label8.Visible = false; label9.Visible = false; label10.Visible = false;
            label11.Visible = false; label16.Visible = false; label17.Visible = false; label18.Visible = false; label19.Visible = false; label26.Visible = false;
            label27.Visible = false; label28.Visible = false; label29.Visible = false; label30.Visible = false; label31.Visible = false; label60.Visible = false;
            label61.Visible = false; label62.Visible = false; label63.Visible = false; label64.Visible = false; label67.Visible = false; label68.Visible = false;
            label69.Visible = false; label70.Visible = false; label71.Visible = false; label72.Visible = false; label51.Visible = false; label52.Visible = false;
            label57.Visible = false; label79.Visible = false; label80.Visible = false; label81.Visible = false; label82.Visible = false; label83.Visible = false;
            label34.Visible = false; label35.Visible = false; label36.Visible = false; label37.Visible = false; label55.Visible = false;

            textBox1.Visible = true; textBox2.Visible = false; /* hides textboxes initially */
            
            button6.Enabled = false; /* stops buttons from working before file loaded */ button2.Visible = false; btnUS.Enabled = false; button3.Enabled = false;
            btnSet.Enabled = false; button4.Enabled = false; button5.Enabled = false;

    }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new System.Drawing.Size(1500, 1000); // set window size
            btnSPD.BackColor = Color.LightGreen; /* set chart button colours */ btnSPD.ForeColor = Color.Black; btnPOW.BackColor = Color.LightGreen;
            btnPOW.ForeColor = Color.Black; btnALT.BackColor = Color.LightGreen; btnALT.ForeColor = Color.Black; btnBPM.BackColor = Color.LightGreen;
            btnBPM.ForeColor = Color.Black; btnCAD.BackColor = Color.LightGreen; btnCAD.ForeColor = Color.Black;
            textBox2.Visible = false; label39.Text = "0"; label41.Text = "0"; label44.Text = "0"; label46.Text = "0";
            DataGridViewColumn column = dataGridView1.Columns[0];
            column.Width = 0; // hides chart1 first column, 1234 etc only used for chart2 calculation.   
        }

        private void button1_Click(object sender, EventArgs e) // load file button
        {   
            string chosenfile = "";
            openFD.Title = "Please select HRM file";
            openFD.InitialDirectory = "D:";
            openFD.FileName = "";
            openFD.Filter = "HRM|*.hrm"; // can only select .HRM files

            openFD.ShowDialog(); // open file selection window

            chosenfile = openFD.FileName;
            int counter = 0; // for counting through lines in file
            string line; // selected line of file
            int countlines = 0; // total lines in file
            int hrgather = 0; // for collecting HRData

            int BPM = 0; // current value (in one line)
            int totalBPM = 0; // all values of one type added together
            int BPMavg = 0; // average of one type
            int SPD = 0; // speed
            int totalSPD = 0; // total speed
            int SPDavg = 0; // average speed
            decimal SPDRND = 0; // speed rounded down
            int RPM = 0; // cadence
            int totalRPM = 0; // total cadence
            int RPMavg = 0; // average cadence
            int FT = 0; // altitude
            int totalFT = 0; // total altitude
            int FTavg = 0; // average altitude
            int WAT = 0; // power
            int totalWAT = 0; // total power
            int WATavg = 0; // average power
            int BPMmin = 999; // minimum BPM
            
            TimeSpan timeSpan; // span assigned for time at start of run
            timeSpan = TimeSpan.Zero;
            TimeSpan second; // span used to increment time by whatever the interval is
            second = TimeSpan.Zero;

            // variables for what data has been recorded
            bool SPDbool = false; bool CADbool = false; bool ALTbool = false; bool POWbool = false;

            string length; string lengthNoMS; // total run length / no milliseconds

            if (System.IO.File.Exists(chosenfile) == true)
            {
                label1.Visible = true; btnEU.Enabled = false;  btnUS.Enabled = false;
                // reads file and displays line by line:
                System.IO.StreamReader file =
                   new System.IO.StreamReader(openFD.FileName);
                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 3)
                    {
                        string SMODE = line.Substring(6, 9);
                        string spd = SMODE.Substring(0, 1);
                        if (spd.Contains("1"))
                        {
                            SPDbool = true;
                        }
                        string cad = SMODE.Substring(1, 1);
                        if (cad.Contains("1"))
                        {
                            CADbool = true;
                        }
                        string alt = SMODE.Substring(2, 1);
                        if (alt.Contains("1"))
                        {
                            ALTbool = true;
                        }
                        string pow = SMODE.Substring(3, 1);
                        if (pow.Contains("1"))
                        {
                            POWbool = true;
                        }
                        string lr = SMODE.Substring(4, 1);
                        if (lr.Contains("1"))
                        {
                          //  LRbool = true; power balance could be added here
                        }
                    }
                    if (counter == 4) // reads date
                    {
                        string D = line.Substring(11, 2); string M = line.Substring(9, 2); string Y = line.Substring(5, 4); string DMY = D + "/" + M + "/" + Y;
                        label16.Text = (D+"/"+M+"/"+Y); // sets date label
                        DateTime dt = DateTime.ParseExact(DMY, "dd/MM/yyyy", CultureInfo.InvariantCulture); // time parsed from above
                        monthCalendar1.SetDate(dt); // sets calendar display to run day
                    }

                    if (counter == 5) // reads time
                    {
                        string time = line.Substring(10, 8); string H = time.Substring(0, 2); int h = Int32.Parse(H);  string M = time.Substring(3, 2);
                        int m = Int32.Parse(M);  string S = time.Substring(6, 2);  int s = Int32.Parse(S);
                        label17.Text = time; timeSpan = new TimeSpan(h, m, s); // starting timespan
                    }

                    if (counter == 6) // reads total run length
                    {
                        length = line.Substring(7, 10); // run length
                        lengthNoMS = line.Substring(7, 8); // run length without milliseconds
                        label18.Text = length; // sets label
                        string stripped = Regex.Replace(lengthNoMS, @"\D", ""); // run length no milli, no colons, used below
                        Double s = Convert.ToDouble(stripped); // above string to double value   
                    }

                    if (counter == 7) // reads intervals
                    {
                        if (line.Length == 10) // 1 character intervals
                        { 
                            string interval = line.Substring(9, 1);
                            i = Int32.Parse(interval);
                            label19.Text = interval;
                            second = new TimeSpan(0, 0, i); // creates timespan for interval, used to increment recorded intervals
                        } else
                        if (line.Length == 11) // 2 character intervals
                        { 
                            string interval = line.Substring(9, 2);
                            i = Int32.Parse(interval);
                            label19.Text = interval;
                            second = new TimeSpan(0, 0, i);  // creates timespan for interval, used to increment recorded intervals
                        } else
                        if (line.Length == 12) // 3 character intervals
                        { 
                            string interval = line.Substring(9, 3);
                            i = Int32.Parse(interval);
                            label19.Text = interval;
                            second = new TimeSpan(0, 0, i);  // creates timespan for interval, used to increment recorded intervals
                        }
                     }

                    if (counter == 22) // read weight
                    {
                        string wght = line.Substring(7, 2); weight = Int32.Parse(wght); label55.Text = wght;
                    }

                    if (line.Contains("[HRData]")) // line before raw data
                    {
                        dataline = -1; // line in the datagridview
                        hrgather = counter + 1;  // hrgather is line at which raw data starts
                    }

                  if (counter > hrgather & hrgather > 0)  // code only runs when HRData begins
                    {
                        string[] split = line.Split('\t'); // splits each line by tab character
                        string bpm = (split[0]); // gets value from array
                        BPM = Int32.Parse(bpm); // parses that value into a useable int
                        chart1.Series["Heart Rate"].Points.AddY(BPM); // adds BPM record to chart

                        if (SPDbool == true) // checks SMode to see if a certain variable has been recorded
                        {
                            string spd = (split[1]); // if so, gets value from array..
                            SPD = Int32.Parse(spd); // ..and parses it into a useable int 
                            SPDRND = (decimal)SPD / 10; // calculates  KM/H

                            if (US == true) // if US mode enabled
                            {
                            chart1.Series["Speed"].Points.AddY(SPD); // adds speed value to chart
                            } else if (EU == true)
                            {
                                double EUDST = Convert.ToDouble(SPDRND); // must be double
                                EUDST = EUDST * 0.62137; // calculation for KM into miles
                                decimal EUCALC = (decimal)EUDST;
                                EUCALC = Math.Round(EUCALC, 2);
                                SPDRND = EUCALC;
                                chart1.Series["Speed"].Points.AddY(SPD*0.62137); // adds speed value to chart
                            }
                        }
                        else SPD = 0; // if not, just sets output to 0
                     
                        if (CADbool == true)
                        {
                            string rpm = (split[2]);
                            RPM = Int32.Parse(rpm);
                            chart1.Series["Cadence"].Points.AddY(RPM); // adds cadence value to chart
                        }
                        else RPM = 0;

                        if (ALTbool == true)
                        {
                            string ft = (split[3]);
                            FT = Int32.Parse(ft);
                            chart1.Series["Altitude"].Points.AddY(FT); // adds altitude value to chart
                        }
                        else FT = 0;

                        if (POWbool == true)
                        {
                            string wat = (split[4]);
                            WAT = Int32.Parse(wat);
                            ppower = Math.Pow(WAT, 4);
                            chart1.Series["Power"].Points.AddY(WAT); // adds power value to chart
                        }
                        else WAT = 0;
                       
                        this.dataGridView1.Rows.Add(dataline, timeSpan, SPDRND, RPM, FT, BPM, WAT); // adds a new row of information to the data grid view 1
                        if (BPM < BPMmin)
                        {
                            BPMmin = BPM; // sets a new minimum BPM to be used later
                        }
                        timeSpan = timeSpan.Add(second); // increments the current timespan by whatever the interval is ie. 1 second
                      
                    }

                    counter++; // each time code iterates
                    dataline++; // total lines in datagridview1
                    totalBPM = totalBPM + BPM; /* adds value to its own total */  totalSPD = totalSPD + SPD; totalRPM = totalRPM + RPM;
                    totalFT = totalFT + FT;totalWAT = totalWAT + WAT; totalppower = totalppower + ppower;
                }

                countlines = (Math.Abs(counter - hrgather)) -1; // finds total number of lines in RAW data, used to make averages, -1 for empty line at end of document
                BPMavg = totalBPM / countlines; /* averages a value by taking its total and dividing by the number of lines */ RPMavg = totalRPM / countlines;
                SPDavg = totalSPD / countlines; decimal SPDround = (decimal)SPDavg / 10; // calculates  KM/H
                FTavg = totalFT / countlines; WATavg = totalWAT / countlines;

                var MaxPOW = dataGridView1.Rows.Cast<DataGridViewRow>() // find max power
                        .Max(r => Convert.ToInt32(r.Cells["Power"].Value));
                decimal POWmax = (decimal)MaxPOW;
                label30.Text = MaxPOW.ToString();

                var MaxRPM = dataGridView1.Rows.Cast<DataGridViewRow>() // find max cadence
                        .Max(r => Convert.ToInt32(r.Cells["Cadence"].Value));
                decimal RPMmax = (decimal)MaxRPM;

                var MaxALT = dataGridView1.Rows.Cast<DataGridViewRow>() // find max altitude
                        .Max(r => Convert.ToInt32(r.Cells["Altitude"].Value));
                decimal ALTmax = (decimal)MaxALT;
                label31.Text = MaxALT.ToString();

                var MaxSPD = dataGridView1.Rows.Cast<DataGridViewRow>() // find max speed
                        .Max(r => Convert.ToDecimal(r.Cells["Speed"].Value));
                decimal SPDmax = (decimal)MaxSPD;
                label27.Text = MaxSPD.ToString();

                var MaxBPM = dataGridView1.Rows.Cast<DataGridViewRow>() // find max heart rate
                        .Max(r => Convert.ToInt32(r.Cells["HeartRate"].Value));
                decimal BPMmax = (decimal)MaxBPM;
                label28.Text = BPMmax.ToString();

                label7.Visible = true; /* restores label visibility */ label8.Visible = true; label9.Visible = true; label10.Visible = true; label11.Visible = true;
                label16.Visible = true; label17.Visible = true; label18.Visible = true; label19.Visible = true; label26.Visible = true; label27.Visible = true;
                label28.Visible = true; label29.Visible = true; label30.Visible = true; label31.Visible = true; label55.Visible = true; button2.Visible = true;

                label7.Text = BPMavg.ToString(); // assigns average value to label
                if (US == true) // US mode
                {
                    label8.Text = SPDround.ToString();
                }
                else if (EU == true) // EU mode
                {
                    double EUDST = Convert.ToDouble(SPDround);
                    EUDST = EUDST * 0.62137;
                    decimal EUCALC = (decimal)EUDST;
                    EUCALC = Math.Round(EUCALC, 2);
                    label8.Text = EUCALC.ToString();
                }
                
                label9.Text = RPMavg.ToString(); /* sets avg labels */ label10.Text = FTavg.ToString(); label11.Text = WATavg.ToString(); label29.Text = BPMmin.ToString();

                if (i == 1) // checks interval and sets total distance accordingly
                {
                    decimal total = (countlines * SPDround / 3600);
                    label26.Text = total.ToString();
                } else if (i==2)
                {
                    decimal total = (countlines * SPDround / 7200);
                    label26.Text = total.ToString();
                } else if (i==3)
                {
                    decimal total = (countlines * SPDround / 10800);
                    label26.Text = total.ToString();
                }

                decimal calcSPD = SPDmax * 10; // multiplies max speed by 10 for use below in finding highest total value
                decimal maxdec = new[] { calcSPD, BPMmax, RPMmax, ALTmax, POWmax }.Max(); // finds highest total value out of all categories..
                double max = (double)maxdec;
                chart1.ChartAreas[0].AxisY.Maximum = max; // ..and sets chart Y axis max value accordingly

                seconds = countlines / i; // total seconds of run is lines divided by interval

                Axis yaxis = chart1.ChartAreas[0].AxisY;
                yaxis.Interval = 50; // Y axis intervals of 50 for easy reading of data
                Axis xaxis = chart1.ChartAreas[0].AxisX;
                xaxis.Interval = 300; // X axis intervals of 10 minutes
               
                chart1.ChartAreas[0].AxisX.Maximum = countlines; // chart's final displayed time is total time
                
                // following code block writes custom labels to X axis, ignoring previously entered values. simply for time display purposes.
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(-600, 600, "00:00");  chart1.ChartAreas[0].AxisX.CustomLabels.Add(0, 1200, "00:10");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(600, 1800, "00:20"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(1200, 2400, "00:30");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(1800, 3000, "00:40"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(2400, 3600, "00:50");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(3000, 4200, "01:00"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(3600, 4800, "01:10");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(4200, 5400, "01:20"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(4800, 6000, "01:30");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(5400, 6600, "01:40"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(6000, 7200, "01:50");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(6600, 7800, "02:00"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(7200, 8400, "02:10");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(7800, 9000, "02:20"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(8400, 9600, "02:30");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(9000, 10200, "02:40"); chart1.ChartAreas[0].AxisX.CustomLabels.Add(9600, 10800, "02:50");
                chart1.ChartAreas[0].AxisX.CustomLabels.Add(10200, 11400, "03:00");

                // chart zoom functionality
                chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
                chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
                chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
                chart1.ChartAreas[0].AxisX.ScrollBar.Enabled = false; // prevents user scrolling through selection
                chart1.ChartAreas[0].CursorY.IsUserEnabled = true;
                chart1.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
                chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
                chart1.ChartAreas[0].AxisY.ScrollBar.Enabled = false; 
                chart1.ChartAreas[0].CursorX.Interval = 0.01;
                chart1.ChartAreas[0].AxisX.Minimum = 0; // X axis always starts at 0
               
                btnSet.Enabled = true; button4.Enabled = true; button5.Enabled = true; textBox1.Visible = false; textBox2.Visible = true;
                file.Close(); // close file to stop reading

                avgppower = totalppower / countlines; // average power calculation for..
                NP = Math.Ceiling(Math.Pow(avgppower, (double)1 / 4)); // ..normalised power
                label39.Text = (NP.ToString());

            } else
            {
                MessageBox.Show("No file selected."); // error message when no file selected
            }
            // suspend screen
            Console.ReadLine();
        }

        private void button2_Click(object sender, EventArgs e) // reset button
        {    
            label7.Visible = false; /* disable more label visibility before file loaded */ label1.Visible = false; label8.Visible = false; label9.Visible = false;
            label10.Visible = false; label11.Visible = false; label16.Visible = false; label17.Visible = false; label18.Visible = false; label19.Visible = false;
            label26.Visible = false; label27.Visible = false; label28.Visible = false; label29.Visible = false; label30.Visible = false; label31.Visible = false;
            label60.Visible = false; label61.Visible = false; label62.Visible = false; label63.Visible = false; label64.Visible = false; label67.Visible = false;
            label68.Visible = false; label69.Visible = false; label70.Visible = false; label71.Visible = false; label72.Visible = false; label55.Visible = false;
            label51.Visible = false; label52.Visible = false; label57.Visible = false; label79.Visible = false; label80.Visible = false; label81.Visible = false;
            label82.Visible = false; label83.Visible = false; btnSet.Enabled = false; button4.Enabled = false; button5.Enabled = false; button2.Visible = false;
            button3.Enabled = false; textBox1.Visible = true; textBox2.Visible = false;

            this.dataGridView1.Rows.Clear(); // clear whole data gride
            DateTime now = DateTime.Now; // reset calendar back to today
            monthCalendar1.SetDate(now);

            FTP = 0; ppower = 0; avgppower = 0; totalppower = 0; NP = 0; IF = 0; TSS = 0;
            label39.Text = "0"; label41.Text = "0"; label44.Text = "0"; label46.Text = "0";

            if (US == true) // make sure EU/US buttons reset correctly
            {
                btnUS.Enabled = false;
                btnEU.Enabled = true;
            }
            else
            {
                btnUS.Enabled = true;
                btnEU.Enabled = false;
            }

            foreach (var series in chart1.Series) // reset graph
            {
                series.Points.Clear();
            }
        }

        private void btnUS_Click(object sender, EventArgs e) // US mode for KM/H
        {
            label32.Text = ("US (KM)"); btnUS.Enabled = false; btnEU.Enabled = true; EU = false; US = true;
            dataGridView1.Columns[1].HeaderText = "Speed [KM/H]:"; dataGridView2.Columns[0].HeaderText = "Speed [KM/H]:"; label3.Text = ("Average Speed (KM/H):"); 
            label20.Text = ("Total Distance (KM):"); label21.Text = ("Maximum Speed (KM/H):"); label74.Text = ("Average Speed (KM/H):"); btnSPD.Text = ("Speed (KM/H)");
        }

        private void btnEU_Click(object sender, EventArgs e) // EU mode for MPH
        {
            label32.Text = ("EU (Miles)"); btnUS.Enabled = true; btnEU.Enabled = false;  EU = true; US = false;
            dataGridView1.Columns[1].HeaderText = "Speed [MPH]:"; dataGridView2.Columns[0].HeaderText = "Speed [MPH]:"; label3.Text = ("Average Speed (MPH):");
            label20.Text = ("Total Distance (Miles):"); label21.Text = ("Maximum Speed (MPH):"); label74.Text = ("Average Speed (MPH):");  btnSPD.Text = ("Speed (MPH)");
        }

        private void btnSpeed_Click(object sender, EventArgs e) // enable / disable speed display on graph
        {
            if (showspeed == true)
            {
                showspeed = false;
                chart1.Series["Speed"].Enabled = false;
                btnSPD.BackColor = Color.Red;
                btnSPD.ForeColor = Color.White;
            }
            else
            {
                showspeed = true;
                chart1.Series["Speed"].Enabled = true;
                btnSPD.BackColor = Color.LightGreen;
                btnSPD.ForeColor = Color.Black;
            }
        }

        private void btnCAD_Click(object sender, EventArgs e) // enable / disable cadence display on graph
        {
            if (showcadence == true)
            {
                showcadence = false;
                chart1.Series["Cadence"].Enabled = false;
                btnCAD.BackColor = Color.Red;
                btnCAD.ForeColor = Color.White;
            }
            else
            {
                showcadence = true;
                chart1.Series["Cadence"].Enabled = true;
                btnCAD.BackColor = Color.LightGreen;
                btnCAD.ForeColor = Color.Black;
            }
        }

        private void btnBPM_Click(object sender, EventArgs e) // enable / disable heart rate display on graph
        {
            if (showheartrate == true)
            {
                showheartrate = false;
                chart1.Series["Heart Rate"].Enabled = false;
                btnBPM.BackColor = Color.Red;
                btnBPM.ForeColor = Color.White;
            }
            else
            {
                showheartrate = true;
                chart1.Series["Heart Rate"].Enabled = true;
                btnBPM.BackColor = Color.LightGreen;
                btnBPM.ForeColor = Color.Black;
            }
        }

        private void btnALT_Click(object sender, EventArgs e) // enable / disable altitude display on graph
        {
            if (showaltitude == true)
            {
                showaltitude = false;
                chart1.Series["Altitude"].Enabled = false;
                btnALT.BackColor = Color.Red;
                btnALT.ForeColor = Color.White;
            }
            else
            {
                showaltitude = true;
                chart1.Series["Altitude"].Enabled = true;
                btnALT.BackColor = Color.LightGreen;
                btnALT.ForeColor = Color.Black;
            }
        }

        private void btnPOW_Click(object sender, EventArgs e) // enable / disable power display on graph
        {
            if (showpower == true)
            {
                showpower = false;
                chart1.Series["Power"].Enabled = false;
                btnPOW.BackColor = Color.Red;
                btnPOW.ForeColor = Color.White;
            }
            else
            {
                showpower = true;
                chart1.Series["Power"].Enabled = true;
                btnPOW.BackColor = Color.LightGreen;
                btnPOW.ForeColor = Color.Black;
            }
        }

        private void chart1_MouseUp(object sender, MouseEventArgs e)
        {
            try { 

                int count = 0;
                if (e.Button == MouseButtons.Right)  // when user right clicks chart, it zooms out one step
                {
                    chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset(1);  chart1.ChartAreas[0].AxisY.ScaleView.ZoomReset(1); // reset zoom
                    dataGridView2.DataSource = null;  dataGridView2.Rows.Clear(); // clear datagrid2
                    count = 0; // reset datagrid2 counter
                    label79.Visible = false; label80.Visible = false; label81.Visible = false; label82.Visible = false; label83.Visible = false; // hide labels
                }

                else // user left clicks - used to draw box to make selection for specific averages
                {
                    dataGridView2.DataSource = null; dataGridView2.Rows.Clear(); // reset datagrid2 on zoom in
                    double lower; double upper;
                    double threefour = Convert.ToDouble(label34.Text); double threesix = Convert.ToDouble(label36.Text); // cursor position is stored in a hidden label, then
                    int ThreeFour = Convert.ToInt32(threefour); int ThreeSix = Convert.ToInt32(threesix); // the value of the label is used to construct the data grid view 2

                    if (ThreeFour > ThreeSix) // drawing in one direction
                    {
                        upper = ThreeFour + 1; lower = ThreeSix; // upper and lower boundaries set for data selection
                        foreach (DataGridViewRow row in dataGridView1.Rows) // for each row in dgv2
                        {
                            try
                            {
                                if (row.Cells[0].Value.ToString() == upper.ToString()) // when the upper selection limit is reached..
                                {
                                    lower = 999999999; // ..the lower is set so high it cannot possibly be used again in this selection
                                }
                                else if (row.Cells[0].Value.ToString() == lower.ToString()) // once the lower limit is found..
                                {
                                    this.dataGridView2.Rows.Add(row.Cells[2].Value, row.Cells[3].Value, row.Cells[4].Value, row.Cells[5].Value, row.Cells[6].Value);
                                    lower++; // ..add the row of data to the grid and increase the row counter to keep iterating
                                }
                            }
                            catch { } // error handling

                        }
                    }
                    else if (ThreeSix > ThreeFour) // drawing in the other direction
                    {
                        upper = ThreeSix; lower = ThreeFour;  // upper and lower boundaries set for data selection
                        foreach (DataGridViewRow row in dataGridView1.Rows)  // for each row in dgv2
                        {
                            try
                            {
                                if (row.Cells[0].Value.ToString() == upper.ToString())  // when the upper selection limit is reached..
                                {
                                    lower = 999999999;  // ..the lower is set so high it cannot possibly be used again in this selection
                                }
                                else if (row.Cells[0].Value.ToString() == lower.ToString())  // once the lower limit is found..
                                {
                                    this.dataGridView2.Rows.Add(row.Cells[2].Value, row.Cells[3].Value, row.Cells[4].Value, row.Cells[5].Value, row.Cells[6].Value);
                                    lower++;  // ..add the row of data to the grid and increase the row counter to keep iterating
                                }
                            }
                            catch { }  // error handling

                        }
                    }

                    decimal spd = 0; decimal avgspd = 0; decimal ttlspd = 0; double spdeu = 0; double avgspdeu = 0; double ttlspdeu = 0;
                    int cad = 0; int alt = 0; int hr = 0; int pow = 0; int ttlcad = 0; int ttlalt = 0; int ttlhr = 0; int ttlpow = 0;
                    int avgcad = 0; int avgalt = 0; int avghr = 0; int avgpow = 0; // lots of variables used to determine new selection-based subset of averages
                    label79.Visible = true; label80.Visible = true; label81.Visible = true; label82.Visible = true;  label83.Visible = true;

                    foreach (DataGridViewRow row in dataGridView2.Rows) // now iterates through newly generated subset datagrid
                    {
                        spd = Convert.ToDecimal(row.Cells["Speed2"].Value); cad = Convert.ToInt32(row.Cells["Cadence2"].Value);
                        alt = Convert.ToInt32(row.Cells["Altitude2"].Value); hr = Convert.ToInt32(row.Cells["HeartRate2"].Value);
                        pow = Convert.ToInt32(row.Cells["Power2"].Value); // this block gets values from columns
                        if (EU == true) // MPH
                        {
                            double SPD = decimal.ToDouble(spd);
                            spdeu = SPD * 0.62137;
                            ttlspdeu = ttlspdeu + SPD;
                        }
                        else if (US == true) // KM/H
                        {
                            ttlspd = ttlspd + spd;
                        }

                        ttlcad = ttlcad + cad; ttlalt = ttlalt + alt; ttlhr = ttlhr + hr; ttlpow = ttlpow + pow; // totals of each column used to average below
                        count++; // increase counter
                    }

                    if (US == true) // KM/H
                    {
                        avgspd = ttlspd / (count - 1);
                        avgspd = Math.Round(avgspd, 1);
                        label79.Text = avgspd.ToString();
                    }
                    else if (EU == true) // MPH
                    {
                        avgspdeu = ttlspdeu / (count - 1);
                        avgspdeu = Math.Round(avgspdeu, 1);
                        label79.Text = avgspdeu.ToString();
                    }

                    // averges: minus 1 for empty row at end of DGV2
                    avgcad = ttlcad / (count - 1); avgalt = ttlalt / (count - 1); avghr = ttlhr / (count - 1); avgpow = ttlpow / (count - 1);
                    // set results
                    label80.Text = avgcad.ToString(); label81.Text = avgpow.ToString(); label82.Text = avgalt.ToString(); label83.Text = avghr.ToString();
                }
            }
            catch { } // error handling
        }

        private void chart1_SelectionRangeChanging(object sender, CursorEventArgs e) // while user is making selection, co-ordinates sent to invisible label used above
        {
            label34.Text = chart1.ChartAreas[0].CursorX.SelectionStart.ToString(); label35.Text = chart1.ChartAreas[0].CursorY.SelectionStart.ToString();
            label36.Text = chart1.ChartAreas[0].CursorX.SelectionEnd.ToString(); label37.Text = chart1.ChartAreas[0].CursorY.SelectionEnd.ToString();  
        }

        private void btnSet_Click(object sender, EventArgs e) // set FTP and also calculate power zones
        {
            if (Double.TryParse(txtSet.Text, out FTP)) // user can only enter whole number
            {
                FTP = double.Parse(txtSet.Text); label41.Text = FTP.ToString();
                if (FTP > 0)
                {
                    button3.Enabled = true; // as long as user enters valid number, can then calculate power zones
                }
                percent2 = FTP / 100; // one percent of functional threshold power, used to calculate zones
                // set power zones
                Zone1lower = percent2 * 55; Zone2lower = percent2 * 56;  Zone3lower = percent2 * 76;  Zone4lower = percent2 * 91; Zone5lower = percent2 * 106;
                Zone6lower = percent2 * 121; Zone2upper = percent2 * 75;Zone3upper = percent2 * 90; Zone4upper = percent2 * 105; Zone5upper = percent2 * 120;
                Zone6upper = percent2 * 150;
                label67.Text = "< " + Zone1lower.ToString(); label68.Text = Zone2lower.ToString() + " - " + Zone2upper.ToString();
                label69.Text = Zone3lower.ToString() + " - " + Zone3upper.ToString(); label70.Text = Zone4lower.ToString() + " - " + Zone4upper.ToString();
                label71.Text = Zone5lower.ToString() + " - " + Zone5upper.ToString();  label72.Text = Zone6lower.ToString() + " - " + Zone6upper.ToString();

            } else

            {
                MessageBox.Show("Please enter a whole number."); // user entered a decimal, doesn't work
            }
        }

        private void button3_Click(object sender, EventArgs e) // calculate intensity factor and training stress score
        {
            label67.Visible = true; label68.Visible = true; label69.Visible = true; label70.Visible = true; label71.Visible = true; label72.Visible = true;
            IF = (NP / FTP);
            IF = Math.Round(IF, 2);
            label44.Text = IF.ToString();
            TSS = TSS = ((seconds * NP * IF)/(FTP * 3600) * 100);
            TSS = Math.Round(TSS, 2);
            label46.Text = TSS.ToString();
        }

        private void button5_Click(object sender, EventArgs e) // set resting heart rate
        {
            if (Int32.TryParse(textBox4.Text, out RHR))
            {
                label52.Visible = true; RHR = int.Parse(textBox4.Text); label52.Text = RHR.ToString();
                if (age > 0 & RHR > 0) // if user input all done and valid
                {
                    if (comboBox1.SelectedIndex > -1)
                    {
                        button6.Enabled = true;
                    }
                }
                else { button6.Enabled = false; }
            } else
            {
                MessageBox.Show("Please enter a whole number."); // BPM must be whole number
            }
        }
        private void button4_Click(object sender, EventArgs e) // set age
        {
            if (Int32.TryParse(textBox3.Text, out age))
            {
             age = int.Parse(textBox3.Text);  label51.Visible = true; label51.Text = age.ToString();
            if (age > 0 & RHR > 0) // if user input all done and valid
            {
                if (comboBox1.SelectedIndex > -1)
                {
                    button6.Enabled = true;
                }
            }
            else { button6.Enabled = false; }
            } else
            {
                MessageBox.Show("Please enter a whole number."); // age must be whole number
            }
        }

        private void button6_Click(object sender, EventArgs e) // calculate heart rate zones
        {
            label60.Visible = true; label61.Visible = true; label62.Visible = true; label63.Visible = true; label64.Visible = true;
            double halfage = age / 2; // half user's age
            double lb = weight * 2.2046; // calculate weight in pounds
            double five; five = (lb / 100) * 5; // 5% of user's weight in pounds
            maxHR = 210 - halfage - five; // precise equation for maximum heart rate
            if (this.comboBox1.SelectedItem == "Male") // user is male
            {
                maxHR = maxHR + 4; // males have higher max heart rate
            } else if (this.comboBox1.SelectedItem == "Female") // user is female
            {
                maxHR = maxHR + 0;
            }
            double mxhr = Math.Round(maxHR, 0);  label57.Visible = true; label57.Text = mxhr.ToString();

            percent = mxhr / 100; // 1% of max heart rate used to calculate zones below
            zone1lower = percent * 60; zone2lower = percent * 65; zone3lower = percent * 75; zone4lower = percent * 82; zone5lower = percent * 89;
            zone1upper = percent * 65; zone2upper = percent * 75; zone3upper = percent * 82; zone4upper = percent * 89; zone5upper = percent * 94;
            label60.Text = (zone1lower.ToString()) + " - " + (zone1upper.ToString()); label61.Text = (zone2lower.ToString()) + " - " + (zone2upper.ToString());
            label62.Text = (zone3lower.ToString()) + " - " + (zone3upper.ToString()); label63.Text = (zone4lower.ToString()) + " - " + (zone4upper.ToString());
            label64.Text = (zone5lower.ToString()) + " - " + (zone5upper.ToString());
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) // when user selects gender
        {
            if (age > 0 & RHR > 0)
            {
                if (comboBox1.SelectedIndex > -1)
                {
                    button6.Enabled = true;
                }
            }
            else { button6.Enabled = false; }
        }
    }
}